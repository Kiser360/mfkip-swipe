const fss = require('fs-extra');
const ArtifactService = require('./artifact.service');
const unzip = require('./unzip');
const { join } = require('path');

const publicDir = join(__dirname, '../public');

(async () => {
    const [artifact] = await Promise.all([
        ArtifactService.getAppArtifact('mfkip', 'prod', '2.1.1-SNAPSHOT.ME-782'),
        fss.ensureDir(publicDir),
    ]);
    
    await unzip(artifact, publicDir);
})()
